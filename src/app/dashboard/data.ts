export let categories: any[] = [
    {
        id: 1,
        text: 'Education'
    },
    {
        id: 2,
        text: 'Finance'
    },
    {
        id: 3,
        text: 'Sports'
    },
    {
        id: 4,
        text: 'Medical'
    },
    {
        id: 5,
        text: 'Technology'
    }
];

export let authors: any[] = [
    {
        id: 1,
        text: 'John',
        image_url: ''
    },
    {
        id: 2,
        text: 'Tom',
        image_url: ''
    },
    {
        id: 3,
        text: 'Jackson',
        image_url: ''
    },
    {
        id: 4,
        text: 'Thompson',
        image_url: ''
    },
    {
        id: 5,
        text: 'Jimmy',
        image_url: ''
    }
];

export let countries: any[] = [
    {
        id: 1,
        name: 'India'
    },
    {
        id: 2,
        name: 'Germany'
    },
    {
        id: 3,
        name: 'Indonesia'
    }
]; // Add Country Flag URL in future

export let articles: any[] = [
    {
        title: '',
        country: '',
        description: '',
        image_url: '',
        author: '',
        category: '',
        created_at: ''
    }
]