import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ApiService } from '../api.service';
import * as moment from 'moment';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  allCategories: any[] = [];

  allAuthors: any[] = [];

  allCountries: any[] = [];

  allArticles: any[] = [];

  allArticlesDuplicate: any[] = [];

  articleForm!: FormGroup;

  articleImageBase64: string | undefined = undefined;

  imageError: boolean = false;

  form: any = {};

  loading: boolean = false;

  showModal: boolean = false;
  
  fileName: string | undefined = undefined;

  filterData: any = {
    dateFilter: [],
    categoryFilter: [],
    authorFilter: []
  };


  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'text',
    itemsShowLimit: 5,
    enableCheckAll: false,
    allowSearchFilter: false
  }

  date: any;
  showDrawer: boolean = false;

  editArticle: boolean = false;

  constructor( private rest: ApiService, private fb: FormBuilder, private toast: ToastrService){

  }

  ngOnInit(): void {
    this.articleForm = this.fb.group({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required),
      author: new FormControl('', Validators.required),
      category: new FormControl('', Validators.required),
    });
    this.getArticles();
    this.getCountry();
    this.getCategory();
    this.getAuthor();
  }

  getArticles(){
    this.rest.getArticles().subscribe((data) => {
      this.allArticles = data;
      this.allArticlesDuplicate = data;
      this.showDrawer = false;
      this.loading = false;
      this.resetForm();
    },
    (err) => {
      console.log(err.error);
      this.showError(err.error);
    });
  }

  getCountry(){
    this.rest.getCountry().subscribe((data) => {
      this.allCountries = data;
    },
    (err) => {
      console.log(err.error);
      this.showError(err.error);
    });
  }

  getAuthor(){
    this.rest.getAuthor().subscribe((data) => {
      this.allAuthors = data;
    },
    (err) => {
      console.log(err.error);
      this.showError(err.error);
    });
  }

  getCategory(){
    this.rest.getCategory().subscribe((data) => {
      this.allCategories = data;
    },
    (err) => {
      console.log(err.error);
      this.showError(err.error);
    });
  }

  updateFilter($event: any, type: number): void{
    switch(type){
      case 1:
        // Date Filter
        this.filterData.dateFilter = $event;
        break;
      case 2:
        // Category Filter
        if(this.filterData.categoryFilter?.includes($event.text)){
          this.filterData.categoryFilter = this.filterData.categoryFilter.filter((e: string) => e !== $event.text);
        }else{
          this.filterData.categoryFilter.push($event.text);
        }
        break;
      case 3:
        // Author Filter
        if(this.filterData.authorFilter?.includes($event.text)){
          this.filterData.authorFilter = this.filterData.authorFilter.filter((e: string) => e !== $event.text);
        }else{
          this.filterData.authorFilter.push($event.text);
        }
        break;
    }
    this.applyFilter();
  }

  applyFilter(): void{
    let data = this.allArticlesDuplicate;
    if(this.filterData.categoryFilter?.length > 0){
      const selectedCategories = this.filterData.categoryFilter;
      const filteredData: any[] = [];
      data.forEach((ele: any) => {
        ele.category.forEach((category: any) => {
          if(selectedCategories.indexOf(category.text) !== -1){
            if(!filteredData.includes(ele)){
              filteredData.push(ele);
            }
          }
        });
      });
      data = filteredData;
    }
    if(this.filterData.authorFilter?.length > 0){
      const selectedAuthors = this.filterData.authorFilter;
      data = data.filter((ele: any) => {
        if(selectedAuthors.includes(ele.author)){
          return ele;
        }
      });
    }
    if(this.filterData.dateFilter?.length > 0){
      const selectedDate = this.filterData.dateFilter;
      const startDate = moment(selectedDate[0]);
      const endDate = moment(selectedDate[1]);
      data = data.filter((ele: any) => {
        const date = moment(+ele.created_at);
        if(date.isBetween(startDate, endDate, 'day', '[]')){
          return ele;
        }
      });
    }
    this.allArticles = data;
  }

  getCount(country: string){
    if(country === "all"){
     return this.allArticles.length; 
    }
    const articles = this.allArticles.filter((ele) => ele.country === country);
    return articles.length;
  }

  returnArray(countryName: string): any[]{
    return this.allArticles.filter((article) => article.country === countryName);
  }

  // Getter Functions to access form controls.
  get Title(){
    return this.articleForm.controls['title'];
  }

  get Description(){
    return this.articleForm.controls['description'];
  }

  get Country(){
    return this.articleForm.controls['country'];
  }

  get Author(){
    return this.articleForm.controls['author'];
  }

  get Category(){
    return this.articleForm.controls['category'];
  }

  fileUpload($event: any){
    const file = $event?.target?.files[0];
    if(file){
      this.fileName = file.name.split('.')[0];
      const reader = new FileReader();
      let base64Image = undefined;
      reader.onload = (e: any) => {
        base64Image = e?.target?.result;
        if(base64Image){
          this.imageError = false;
          this.form['image_url'] = base64Image;
          this.form['file_name'] = this.fileName;
        }else{
          this.imageError = true;
        }
      }
      reader.readAsDataURL(file);
    }
  }

  addNewArticle(edit: boolean = false){
    this.loading = true;
    if(!this.fileName){
      this.imageError = true;
      this.loading = false;
    }
    if(this.articleForm.valid){
      if(!this.fileName){
        return;
      }
      const userInput = this.articleForm.value;

      // Add Extra Info
      this.form['id'] = this.form.length + (edit ? 0 : 1);
      this.form['created_at'] = new Date().getTime();
      const country = userInput.country;
      this.form['country_code'] = (country === 'Indonesia' ? 'ID' : country === 'Germany' ? 'DE' : 'IN');
      const author = userInput.author;

      // Check if existing author
      const existing_author = (this.allAuthors.filter(e => e.text === author)).length > 0;
      if(!existing_author){
        this.addNewAuthor(author);
      }

      Object.entries(userInput).forEach((value) => {
        // if(typeof value[1] === 'object'){
        //   this.form[value[0]] = userInput.category.map((e: any) => e.text ? e.text : e);
        // }else{
          this.form[value[0]] = value[1];
        // }
      });
      if(edit){
        this.saveEditedArticle();
      }else{
        this.uploadArticle();
      }
    }else{
      Object.keys(this.articleForm.controls).forEach( (key) => {
        this.articleForm.get(key)?.markAsDirty();
      });
      this.loading = false;
    }
  }

  addNewAuthor(name: string){
    const data = {
      id: this.allAuthors.length + 1,
      text: name,
      image_url: ''
    };
    this.rest.createAuthor(data).subscribe((res) => {
      this.getAuthor();
    }, (err) => {
      console.log(err);
      this.showError(err.error);
    });
  }

  uploadArticle(){
    this.rest.createArticle(this.form).subscribe((res) => {
      this.getArticles();
      this.showSuccess('Article Created Successfully!');
    }, (err) => {
      console.log(err);
      this.showError(err.error);
    });
  }

  cancel(){
    this.editArticle = false;
    this.form = {};
    this.resetForm();
    this.showDrawer = false;
  }

  deleteArticle(data: any): void{
    if(typeof data !== 'boolean'){
      // Open Drawer
      this.populateData(data);
    }else{
      // Delete API Call
      this.rest.deleteArticle().subscribe(res => {
        this.getArticles();
        this.showSuccess('Article Deleted Successfully!');
      },
      (err) => {
        console.log(err);
        this.showError(err.error);
      });
    }
  }

  resetForm(){
    this.articleForm.reset();
    this.articleForm.markAsPristine();
    this.fileName = undefined;
    $('#upload').val(null);
  }

  populateData(data: any): void{
    this.editArticle = true;
    this.articleForm.patchValue(data);
    this.fileName = data?.file_name;
    this.form['image_url'] = data?.image_url;
    this.form['file_name'] = this.fileName;
    this.showDrawer = true;
  }

  saveEditedArticle(): void{
    this.rest.editArticle(this.form).subscribe((res) => {
      this.getArticles();
      this.showSuccess('Article Edited Successfully!');
      this.editArticle = false;
    },
    (err) => {
      console.log(err);
      this.showError(err.error);
    })
  }

  clearUploadedImage(){
    this.fileName = undefined;
  }

  showError(msg?: string){
    this.loading = false;
    this.toast.error("", msg ? msg : 'Something went wrong!');
  }

  showSuccess(msg: string){
    setTimeout(() => {
      this.toast.success("", msg);
    },200);
  }
}
