import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private article_id: number = 0;

  constructor(private http: HttpClient) { }

  getArticles(): Observable<any>{
    return this.http.get("http://localhost:3000/articles");
  }

  getCategory(): Observable<any>{
    return this.http.get("http://localhost:3000/category");
  }

  getAuthor(): Observable<any>{
    return this.http.get("http://localhost:3000/author");
  }

  getCountry(): Observable<any>{
    return this.http.get("http://localhost:3000/country");
  }

  createArticle(data: any): Observable<any>{
    return this.http.post("http://localhost:3000/articles", data);
  }

  createAuthor(data: any): Observable<any>{
    return this.http.post("http://localhost:3000/author", data);
  }

  deleteArticle(): Observable<any>{
    return this.http.delete(`http://localhost:3000/articles/${this.article_id}`);
  }

  editArticle(data: any): Observable<any>{
    return this.http.put(`http://localhost:3000/articles/${this.article_id}`, data);
  }

  public get articleId(){
    return this.article_id;
  }

  public set articleId(value: number){
    this.article_id = value;
  }
}
