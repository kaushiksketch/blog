import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  showModal: boolean = false;
  constructor(private rest: ApiService){}
  @Input() allArticles: any[] = [];
  @Output() delete = new EventEmitter();

  ngOnInit(): void {
    this.sortArticles();
  }

  sortArticles(){
    this.allArticles.sort((a,b) => {
      const first = moment(+a.created_at);
      const second = moment(+b.created_at);
      return first.isBefore(second, 'hour') ? 1 : -1;
    });
  }

  returnDate(milliseconds: string): string{
    return moment(+milliseconds).format('MMM Do YYYY');
  }

  emitEvent(confirm: boolean): void{
    if(confirm){
      this.delete.emit(true);
    }
    this.showModal = false;
  }

  toggleModal(id: number, edit?: boolean, data?: any){
    this.rest.articleId = id;
    if(edit){
      this.delete.emit(data);
      return;
    }
    this.showModal = true;
  }
}
